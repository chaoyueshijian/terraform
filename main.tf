terraform {
  required_providers {
    tencentcloud = {
      source = "tencentcloudstack/tencentcloud"
    }
  }
}

# Configure the TencentCloud Provider
provider "tencentcloud" {
  region = "ap-shanghai"
}


resource "tencentcloud_vpc" "foo" {
  name         = "vpc-sandload-terraform-test"
  cidr_block   = "10.0.0.0/16"
  dns_servers  = ["183.60.83.19", "183.60.82.98"]
  is_multicast = false
  tags = {
    "test" = "testassdd"
  }
}

resource "tencentcloud_subnet" "foo" {
  vpc_id            = tencentcloud_vpc.foo.id
  availability_zone = "ap-shanghai-4"
  name              = "vpc-sandload-terraform-test"
  cidr_block        = "10.0.1.0/24"
}



resource "tencentcloud_kubernetes_cluster" "managed_cluster" {
  vpc_id                  = tencentcloud_vpc.foo.id
  cluster_cidr            = "172.16.0.0/20"
  cluster_max_pod_num     = 32
  cluster_name            = "terraform-test"
  cluster_desc            = "terraform-test desc"
  cluster_max_service_num = 512
  cluster_version         = "1.24.4"
  cluster_internet        = true
  # managed_cluster_internet_security_policies = ["3.3.3.3", "1.1.1.1"]
  cluster_deploy_type = "MANAGED_CLUSTER"
  container_runtime   = "containerd"


  labels = {
    "test1" = "test1",
    "test2" = "test2",
  }
}



variable "default_instance_type" {
  default = "SA2.MEDIUM4"
}

variable "default_backup_instance_types" {
  default = ["SA2.LARGE4", "SA2.LARGE8", "SA2.LARGE16"]
}

//this is one example of managing node using node pool
resource "tencentcloud_kubernetes_node_pool" "mynodepool" {
  name                     = "mynodepool"
  cluster_id               = tencentcloud_kubernetes_cluster.managed_cluster.id
  max_size                 = 2
  min_size                 = 2
  vpc_id                   = tencentcloud_vpc.foo.id
  subnet_ids               = [tencentcloud_subnet.foo.id]
  retry_policy             = "INCREMENTAL_INTERVALS"
  desired_capacity         = 2
  enable_auto_scale        = false
  multi_zone_subnet_policy = "EQUALITY"

  auto_scaling_config {
    instance_type         = var.default_instance_type
    backup_instance_types = var.default_backup_instance_types
    system_disk_type      = "CLOUD_PREMIUM"
    system_disk_size      = "50"
    security_group_ids    = ["sg-gwxssjvq"]

    data_disk {
      disk_type = "CLOUD_PREMIUM"
      disk_size = 50
    }

    internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 10
    public_ip_assigned         = true
    password                   = "test123#"
    enhanced_security_service  = false
    enhanced_monitor_service   = false
    host_name                  = "12.123.0.0"
    host_name_style            = "ORIGINAL"
  }
}
